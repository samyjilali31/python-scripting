#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import re
import os, sys


#version corriger os

def strextract(dir, suffix, path, all):
     for root, _, filenames in os.walk(dir):
        for filename in filenames:
          if suffix  and not filename.endswith(suffix):
            continue
          if not all and filename.startswith("."):
            continue
          
        # filtrer filename par rapport à --all et --suffix
          filepath = os.path.join(root,filename)
          with open (filepath) as document:
            
            for line in document:
              match_list = re.findall(r"['\"](.*?)['\"]", line)
              
              for match in match_list:
                if path:
                  print(filepath, match) 
                else:
                  print(match)  


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("dir", type=str, help="directory path")
    parser.add_argument("-s", "--suffix", type=str, help="suffix of to be deleted files")
    parser.add_argument("-a", "--all", action='store_true', help="Include the hidden files (.)")
    parser.add_argument("--path", action='store_true', help="Show the Path info")

    args = parser.parse_args()
   

    strextract(args.dir,args.suffix,args.path,args.all)
                            
# comment
if __name__ == '__main__':
    main()

   