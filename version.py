#!/usr/bin/env python3
# -- coding: utf-8 --

import os, sys, argparse, re

def strextract(dir, suffix, show_path, show_all):

    regex_compiled_version = r"['\"](.*?)['\"]"     # print(regex_compiledversion)
    for root,_ , filenames in os.walk(dir):
        for filename in filenames:

            if suffix and not filename.endswith(suffix):
                continue
            if not show_all and filename.startswith('.'):
                 continue

            try:
                filepath = os.path.join(root, filename)
                with open(filename) as file:
                    for line in file:
                        list_of_found_items = re.findall(regex_compiled_version, line)
                        for found_item in list_of_found_items:
                            if show_path:
                                print(filepath, '\t"', found_item, '"', sep='')
                            else:
                                print('"', found_item, '"', sep='')

            except : # FileNotFoundError
                pass # print("ERROR: There is clearly some error here...")

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("dir", type=str, help="Directoty path for search")
    parser.add_argument("-s", "--suffix", type=str, default="", help="Search files with mask (ex.: '.txt')")
    parser.add_argument("-a", "--all", action='store_true', help="Include the hidden files (.)")
    parser.add_argument("--path", action='store_true', help="Show the Path info")

    args = parser.parse_args()

    strextract(args.dir, args.suffix, args.path, args.all)

if __name__ == 'main':
    main()