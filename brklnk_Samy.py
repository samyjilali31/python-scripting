from bs4 import BeautifulSoup
from urllib import *
import argparse
import requests
from urllib.parse import *


def get_links(url):

    request_get = requests.get(url)

    soup = BeautifulSoup(request_get.text, 'html.parser')

    links_in_page = soup.find_all('a')

    return links_in_page

def analyse(url, depth, site):

    if depth >= False:

            if url not in site:

                request = requests.get(url)

                if request.status_code > 399:
                    print("Le lien",url,"est cassé, erreur:",request.status_code)

            if depth >= True:

                links = get_links(url)

                for link in links:

                        href = urljoin(url, link.get('href'))

                        analyse(href, depth-1, site)    


def main():
   
    parser = argparse.ArgumentParser()

    parser.add_argument("url", type=str, help="Put the url.")
    parser.add_argument("--depth", default="1", type=int,  help="")
    args = parser.parse_args()

    in_link = set()
    analyse(args.url, args.depth, in_link)

if __name__ == "__main__":
    main()